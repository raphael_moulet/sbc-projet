package sbc.drugMiner.app.main;

import sbc.drugMiner.app.deepLearning.Algorithme;
import sbc.drugMiner.app.knowledgeDB.Normalizer;
import sbc.drugMiner.app.knowledgeDB.Parser;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		try {
			Normalizer norm = new Parser("donnees-defi-egc.csv").submitAll().normalize().run().cleanup();
			// new Algorithme().launch(norm.getPath()); // not yet
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
