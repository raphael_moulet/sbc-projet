package sbc.drugMiner.app.deepLearning;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.AutoEncoder;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Algorithme {
	private long seed = 6;
	final int numInputs = 29;
	private int iterations = 5000;// best 5000
	private int outputNum = 2;
	private DataSet trainingData;
	private DataSet testData;
	private static Logger log = LoggerFactory.getLogger(Algorithme.class);

	public void initialize(String path) throws FileNotFoundException, IOException, InterruptedException {
		// First: get the dataset using the record reader. CSVRecordReader
		// handles loading/parsing
		int numLinesToSkip = 0;
		String delimiter = ",";
		RecordReader recordReader = new CSVRecordReader(numLinesToSkip, delimiter);
		recordReader.initialize(new FileSplit(new ClassPathResource(path).getFile()));

		// Second: the RecordReaderDataSetIterator handles conversion to DataSet
		// objects, ready for use in neural network
		int labelIndex = 29; // 5 values in each row of the iris.txt CSV: 4
								// input features followed by an integer label
								// (class) index. Labels are the 5th value
								// (index 4) in each row
		int numClasses = 2; // 3 classes (types of iris flowers) in the iris
							// data set. Classes have integer values 0, 1 or 2
		int batchSize = 15375; // Iris data set: 150 examples total. We are
								// loading all of them into one DataSet (not
								// recommended for large data sets)

		DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize, labelIndex, numClasses);
		DataSet allData = iterator.next();
		allData.shuffle();
		SplitTestAndTrain testAndTrain = allData.splitTestAndTrain(0.75); // Use
																			// 65%
																			// of
																			// data
																			// for
																			// training

		trainingData = testAndTrain.getTrain();
		testData = testAndTrain.getTest();

		// We need to normalize our data. We'll use NormalizeStandardize (which
		// gives us mean 0, unit variance):
		DataNormalization normalizer = new NormalizerStandardize();
		normalizer.fit(trainingData); // Collect the statistics (mean/stdev)
										// from the training data. This does not
										// modify the input data
		normalizer.transform(trainingData); // Apply normalization to the
											// training data
		normalizer.transform(testData); // Apply normalization to the test data.
										// This is using statistics calculated
										// from the *training* set
	}

	public MultiLayerConfiguration getConfig() {
		log.info("Build model....");
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed)
				.gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
				.gradientNormalizationThreshold(1.0).iterations(iterations).momentum(0.5)
				.momentumAfter(Collections.singletonMap(3, 0.9))
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).learningRate(1e-6)
				// .regularization(true).l2(1e-1)
				.list()
				.layer(0,
						new AutoEncoder.Builder().nIn(29).nOut(29).weightInit(WeightInit.XAVIER)
								.lossFunction(LossFunction.MCXENT).corruptionLevel(0.4).build())
				.layer(1,
						new AutoEncoder.Builder().nIn(29).nOut(29).weightInit(WeightInit.XAVIER)
								.lossFunction(LossFunction.MCXENT).corruptionLevel(0.5).build())
				.layer(2,
						new AutoEncoder.Builder().nIn(29).nOut(29).weightInit(WeightInit.XAVIER)
								.lossFunction(LossFunction.MCXENT).corruptionLevel(0.2).build())
				.layer(3,
						new OutputLayer.Builder(LossFunction.MEAN_SQUARED_LOGARITHMIC_ERROR)
								.activation(Activation.SOFTMAX).nIn(29).nOut(outputNum).build())
				.pretrain(false).backprop(true).build();
		log.info("Building model done");
		return conf;
	}

	public MultiLayerConfiguration getBestConfig() {
		log.info("Build model....");
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed).momentum(0.5)
				.momentumAfter(Collections.singletonMap(3, 0.9))
				.optimizationAlgo(OptimizationAlgorithm.CONJUGATE_GRADIENT)// very
																			// good
																			// performance
																			// CONJUGATE_GRADIENT
				.iterations(iterations).activation(Activation.TANH) // very good
																	// performance
																	// : tanh
				.weightInit(WeightInit.XAVIER).learningRate(0.1).regularization(true).l2(1e-4).list()
				.layer(0, new AutoEncoder.Builder().nIn(numInputs).nOut(3).corruptionLevel(0.3).build())
				.layer(1, new AutoEncoder.Builder().nIn(3).nOut(3).corruptionLevel(0.3).build())
				.layer(2, new AutoEncoder.Builder().nIn(3).nOut(3).corruptionLevel(0.3).build())
				.layer(3, new AutoEncoder.Builder().nIn(3).nOut(3).corruptionLevel(0.3).build())
				.layer(4,
						new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
								.activation(Activation.SOFTMAX).nIn(3).nOut(outputNum).build())
				.backprop(true).pretrain(true).build();
		log.info("Building model done");

		return conf;
	}

	public MultiLayerNetwork runModel(MultiLayerConfiguration conf) {
		log.info("Run model....");
		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		model.setListeners(new ScoreIterationListener(1000));

		model.fit(trainingData);
		return model;
	}

	public void evaluate(MultiLayerNetwork model) {
		// evaluate the model on the test set
		log.info("Evaluate model....");
		Evaluation eval = new Evaluation(2);
		INDArray output = model.output(testData.getFeatureMatrix());
		eval.eval(testData.getLabels(), output);
		log.info(eval.stats());
	}

	public void launch(String path) throws FileNotFoundException, IOException, InterruptedException {
		this.initialize(path);
		this.evaluate(this.runModel(this.getBestConfig()));
	}
}
