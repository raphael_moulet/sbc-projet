package sbc.drugMiner.app.knowledgeDB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.web.HttpOp;


public class Pgxlod {
	
	public static void execSelectAndPrint(String serviceURI, String query, HttpClient client) {
	   	 QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI,
	   			 query, client);
	   	 ResultSet results = q.execSelect();
	
	   	 ResultSetFormatter.out(System.out, results);
	
	   	 while (results.hasNext()) {
	   		 QuerySolution soln = results.nextSolution();
	   		 RDFNode x = soln.get("x");
	   		 System.out.println(x);
	   	 }
	    }
	
	    public static void execSelectAndProcess(String serviceURI, String query) {
	   	 QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI,
	   			 query);
	   	 ResultSet results = q.execSelect();
	
	   	 while (results.hasNext()) {
	   		 QuerySolution soln = results.nextSolution();
	   		 // assumes that you have an "?x" in your query
	   		 RDFNode x = soln.get("x");
	   		 System.out.println(x);
	   	 }
	    }
	    
	    private static String getServiceURI() {
	    	return "https://pgxlod.loria.fr/bigdata/namespace/kb/sparql";
	    }
	    
	    private static String getQuery(String filename) throws IOException {
	    	String path = "./Requetes/" + filename;
	    	
	    	BufferedReader reader = new BufferedReader(new FileReader (path));
	        String         line = null;
	        StringBuilder  stringBuilder = new StringBuilder();
	        String         ls = System.getProperty("line.separator");

	        try {
	            while((line = reader.readLine()) != null) {
	                stringBuilder.append(line);
	                stringBuilder.append(ls);
	            }
	            return stringBuilder.toString();
	        } finally {
	            reader.close();
	        }
	    }
	    
	    public static HttpClient getClient() {
	    	CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    	Credentials credentials = new UsernamePasswordCredentials("student","5hoPpeR4");
	    	credsProvider.setCredentials(AuthScope.ANY, credentials);
	    	HttpClient httpclient = HttpClients.custom()
	    	    .setDefaultCredentialsProvider(credsProvider)
	    	    .build();
	    	HttpOp.setDefaultHttpClient(httpclient);
	    	
	    	return httpclient;
	    }
	
	    public static void main(String[] argv) throws IOException {
	   	 // uploadRDF(new File("test.rdf"), );
	    
	    String serviceURI = getServiceURI();
	    String query = getQuery("Exemple2");
	    //System.out.println(query);
	    HttpClient client = getClient();
	
	   	 execSelectAndPrint(
	   			 serviceURI, query, client);
	   	 /*
	   	 execSelectAndProcess(
	   			 "http://localhost:3030/test",
	   			 "SELECT ?x WHERE { ?x  <http://www.w3.org/2001/vcard-rdf/3.0#FN>  \"John Smith\" }");
	   			 */
	    }

}
