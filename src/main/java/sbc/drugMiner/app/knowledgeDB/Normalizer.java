package sbc.drugMiner.app.knowledgeDB;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.TransformProcess.Builder;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.util.ClassPathResource;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Normalizer {

	private HashMap<String, List<String>> columns;
	private String outputPath;
	private String inputPath;
	private TransformProcess tp;
	private ArrayList<String> index;
	private String path;
	private static Logger log = LoggerFactory.getLogger(Normalizer.class);

	public Normalizer(String path, HashMap<String, List<String>> columns, ArrayList<String> index) {
		this.index = index;
		this.columns = columns;
		this.path = path;
		try {
			inputPath = new ClassPathResource(path).getFile().getPath();
			outputPath = new ClassPathResource(path).getFile().getPath() + "_normalized_"
					+ String.valueOf(new Date().getTime());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Normalizer run() throws Exception {
		System.setProperty("hadoop.home.dir", "C:\\winutils");

		log.info("running normalizer..");

		org.datavec.api.transform.schema.Schema.Builder schemaBuilder = new Schema.Builder();
		for (int j = 0; j < index.size(); j++)
			schemaBuilder.addColumnsString(index.get(j));

		Schema inputDataSchema = schemaBuilder.build();

		/**
		 * Define a transform process to extract lat and lon and also transform
		 * type from one of three strings to either 0,1,2
		 */
		Builder builder = new TransformProcess.Builder(inputDataSchema);
		for (int j = 0; j < index.size(); j++)
			builder.stringToCategorical(index.get(j), columns.get(index.get(j))).categoricalToInteger(index.get(j));

		tp = builder.build();

		/**
		 * Some code to step through and print the before and after Schema
		 */
		SparkConf sparkConf = new SparkConf();
		sparkConf.setMaster("local[*]");
		sparkConf.setAppName("deep learning in a knowledge database");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		/**
		 * Get our data into a spark RDD and transform that spark RDD using our
		 * transform process
		 */
		// read the data file
		JavaRDD<String> lines = sc.textFile(inputPath);
		final String first = lines.first();
		lines = lines.filter(line -> !line.equals(first));

		// convert to Writable
		JavaRDD<List<Writable>> data = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
		// run our transform process
		JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(data, tp);
		// convert Writable back to string for export
		JavaRDD<String> toSave = processed.map(new WritablesToStringFunction(","));

		toSave.saveAsTextFile(outputPath);
		sc.close();

		return this;
	}

	public Normalizer cleanup() throws IllegalArgumentException, IOException {
		Configuration hadoopConfig = new Configuration();
		FileSystem hdfs = FileSystem.get(hadoopConfig);
		if (hdfs.exists(new Path(inputPath + "_train"))) {
			hdfs.delete(new Path("." + inputPath + "_train"), true);
		}
		FileUtil.copyMerge(hdfs, new Path(outputPath), hdfs, new Path(inputPath + "_train"), false, hadoopConfig, null);
		hdfs.delete(new Path(outputPath), true);

		return this;
	}

	public String getPath() {
		return path + "_train";
	}

	public TransformProcess getTp() {

		return tp;
	}
}