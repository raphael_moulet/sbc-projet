package sbc.drugMiner.app.knowledgeDB;

import java.io.IOException;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.riot.web.HttpOp;

public class DBMiner {

	private String serviceURI = new String("https://pgxlod.loria.fr/bigdata/namespace/kb/sparql");
	private HttpClient client;
	private ParameterizedSparqlString pss;
	private String queryBody = new String("CONSTRUCT {}\n" + "WHERE {pharmgkb:paramDrug pharmgkbv:x-drugbank ?drug2.\n"
			+ "pharmgkb:paramDrug pharmgkbv:x-umls ?cui.\n"
			+ "pharmgkb:paramDrug pharmgkbv:x-pubchemcompound ?compound.\n"
			+ "?drug3 siderv:pubchem-flat-compound-id ?compound. \n" + "?drug_target dbv:drug ?drug2.\n"
			+ "?drug_target dbv:action ?action.\n" + "?drug2 dbv:x-pubchemcompound ?compound. \n"
			+ "?drug2 dbv:x-atc ?atc. \n" + "?drug_target dbv:target ?target.\n" + "?target dbv:x-uniprot ?prot.\n"
			+ "pharmgkb:paramGene pharmgkbv:x-uniprot ?prot.\n" + "pharmgkb:paramGene pharmgkbv:x-ncbigene ?gene.\n"
			+ "?gene2 clinvarv:x-gene ?gene.\n" + "?gene2 clinvarv:x-sequence_ontology ?so.\n"
			+ "?rcv clinvarv:Variant_Gene ?gene2.\n" + "?rcv clinvarv:Variant_Phenotype ?x. \n"
			+ "?x clinvarv:x-medgen ?disease2. \n" + "?gene bio2rdfv:x-identifiers.org ?gene3.\n"
			+ "?var sio:SIO_000628 ?gene3.\n" + "?var sio:SIO_000628 ?disease. \n" + "?gene3 sio:SIO_000062 ?react.\n"
			+ "?disease sio:SIO_000095 ?mesh.\n" + "?disease sio:SIO_000008 ?semantic_type.\n"
			+ "?disease skos:exactMatch ?disease2. \n" + "?drug3 siderv:side-effect ?disease3.\n"
			+ "?disease skos:exactMatch ?disease3. \n" + "?disease4 mapping:medispan_to_sider ?disease3. \n"
			+ "?disease2 mapping:clinvar_to_sider ?disease3.\n" + "?disease2 mapping:clinvar_to_medispan ?disease4.\n"
			+ "}");

	public DBMiner() {
		this.client = this.getClient();
		this.pss = this.buildQuery();
	}

	private HttpClient getClient() {
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		Credentials credentials = new UsernamePasswordCredentials("student", "5hoPpeR4");
		credsProvider.setCredentials(AuthScope.ANY, credentials);
		HttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		HttpOp.setDefaultHttpClient(httpclient);

		return httpclient;
	}

	private ParameterizedSparqlString buildQuery() {
		ParameterizedSparqlString pss = new ParameterizedSparqlString();
		pss.setNsPrefix("atc", "http://bio2rdf.org/atc:");
		pss.setNsPrefix("bio2rdfv", "http://bio2rdf.org/bio2rdf_vocabulary:");
		pss.setNsPrefix("clinvar", "http://bio2rdf.org/clinvar:");
		pss.setNsPrefix("clinvarv", "http://bio2rdf.org/clinvar_vocabulary:");
		pss.setNsPrefix("dbv", "http://bio2rdf.org/drugbank_vocabulary:");
		pss.setNsPrefix("disgenet", "http://rdf.disgenet.org/resource/gda/");
		pss.setNsPrefix("drugbank", "http://bio2rdf.org/drugbank:");
		pss.setNsPrefix("mapping", "http://biodb.jp/mappings/");
		pss.setNsPrefix("medispan", "http://orpailleur.fr/medispan/");
		pss.setNsPrefix("ncbigene", "http://bio2rdf.org/ncbigene:");
		pss.setNsPrefix("pharmgkb", "http://bio2rdf.org/pharmgkb:");
		pss.setNsPrefix("pharmgkbv", "http://bio2rdf.org/pharmgkb_vocabulary:");
		pss.setNsPrefix("pubchemcompound", "http://bio2rdf.org/pubchem.compound:");
		pss.setNsPrefix("sider", "http://bio2rdf.org/sider:");
		pss.setNsPrefix("siderv", "http://bio2rdf.org/sider_vocabulary:");
		pss.setNsPrefix("sio", "http://semanticscience.org/resource/");
		pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
		pss.setNsPrefix("so", "http://bio2rdf.org/sequence_ontology:");
		pss.setNsPrefix("umls", "http://bio2rdf.org/umls:");
		pss.setNsPrefix("uniprot", "http://bio2rdf.org/uniprot:");
		return pss;
	}

	public ResultSet executeCommand(String drug, String gene) {
		queryBody = queryBody.replaceAll("paramDrug", drug).replaceAll("paramGene", gene);
		pss.setCommandText(queryBody);

		QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, pss.toString(), client);
		ResultSet results = q.execSelect();

	//	ResultSetFormatter.out(System.out, results);

		while (results.hasNext()) {
			QuerySolution soln = results.nextSolution();
			System.out.println(soln.get("predicate"));
//			while(var.hasNext())
//				System.out.println(var.next());

		//	System.out.println(x.asNode().getLiteral());
		}
		return results;
	}

	public static void main(String[] argv) throws IOException {
		new DBMiner().executeCommand("PA131301952", "PA7360");
	}

}
