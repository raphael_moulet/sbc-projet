package sbc.drugMiner.app.knowledgeDB;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.jena.query.ResultSet;
import org.datavec.api.util.ClassPathResource;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Parser {

	private DBMiner dbMiner;
	private String path;
	private boolean first = false;
	private HashMap<String, List<String>> columns = new HashMap<>();
	private ArrayList<String> index = new ArrayList<>();
	private static Logger log = LoggerFactory.getLogger(Parser.class);

	public Parser(String path) {
		this.path = path;
		this.dbMiner = new DBMiner();
	}

	public Parser submitAll() {
		log.info("Begin parsing ...");
		String line = "";
		String tsvSplitBy = ",";
		try (BufferedReader br = new BufferedReader(new FileReader(new ClassPathResource(path).getFile()))) {
			// try (BufferedWriter bw = new BufferedWriter(new FileWriter(new
			// ClassPathResource(path).getFile()+"_to_train"))){

			line = br.readLine();
			for (String tab : line.split(tsvSplitBy)) {
				columns.put(tab, new ArrayList<String>());
				index.add(tab);
				if (!first)
					tab = "," + tab;
				else
					first = true;
				// bw.write(tab);
			}
			// bw.newLine();
			while ((line = br.readLine()) != null) {
				// use comma as separator
				String[] tabs = line.split(tsvSplitBy);
				int i = 0;
				for (String tab : tabs) {
					String key = index.get(i++);
					if (!columns.get(key).contains(tab))
						columns.get(key).add(tab);
				}

				// bw.write(constructLine(dbMiner.executeCommand(tabs[0],
				// tabs[1])));
				// bw.write(tabs[2]);
				// bw.newLine();

				// }
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		log.info("parsing done");

		return this;
	}

	public Normalizer normalize() throws Exception {
		log.info("normalizing..");
		return new Normalizer(this.path/* +"_to_train" */, columns, index);
	}

	private String constructLine(ResultSet result) {
		return "";
	}

	public static void main(String[] args) throws Exception {
		new Parser("donnees-defi-egc.csv").submitAll().normalize().run();
	}
}
